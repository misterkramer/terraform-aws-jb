# terraform-aws-jb

[![Conventional Commits](https://img.shields.io/badge/Conventional%20Commits-1.0.0-yellow.svg)](https://conventionalcommits.org)

All AWS stuff for [JB demo project](https://gitlab.com/misterkramer/cutter).

Enjoe :sunglasses:  

## Project structure

```text
.
├── README.md
├── infra.drawio
├── infra.png
├── main.tf
├── modules
│   └── cutter
│       ├── README.md
│       ├── module.tf
│       └── variables.tf
├── variables.tf
└── versions.tf
```

Проект следует [medium-size-infrastructure](https://www.terraform-best-practices.com/examples/terraform/medium-size-infrastructure).

## HOW TO USE

В качестве хранения и лока `terrraform state` используется [gitlab backend http](https://docs.gitlab.com/ee/user/infrastructure/terraform_state.html).

### Local

``` bash
export TF_HTTP_USERNAME=$gitlab_username
export TF_HTTP_PASSWORD=$gitlab_token # https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html
# set init terraform configuration
terraform init
# plan
terraform plan
# apply
terraform apply
```

### GITLAB-CI

Используется следующий [template](https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Terraform/Base.latest.gitlab-ci.yml).

Подробнее про интеграцию можно почитать [тут](https://docs.gitlab.com/ce/user/infrastructure/).

CI запускается на каждый коммит.

`terraform apply` запускается только из мастера вручную в `deploy stage`.

#### CI Variables

```text
AWS_ACCESS_KEY_ID
AWS_SECRET_ACCESS_KEY
TF_VAR_hashids_salt
```

## List of modules

Смотри [тут](./modules/cutter/README.md)

## Architecture

![Диаграмма png](infra.png)
[Диаграмма drawio](infra.drawio)

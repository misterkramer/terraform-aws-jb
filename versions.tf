terraform {
  required_version = ">= 0.13"

  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/25639602/terraform/state/main"
    lock_address   = "https://gitlab.com/api/v4/projects/25639602/terraform/state/main/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/25639602/terraform/state/main/lock"
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = "5"
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.35.0"
    }
  }
}

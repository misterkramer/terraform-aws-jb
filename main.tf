provider "aws" {
  region = "us-west-2"
}

provider "aws" {
  alias  = "eu_west_2"
  region = "eu-west-2"
}

resource "aws_ecr_repository" "cutter" {
  name = "cutter"

  image_scanning_configuration {
    scan_on_push = true
  }
}

module "dynamodb_table" {
  source  = "terraform-aws-modules/dynamodb-table/aws"
  version = "0.13.0"

  name             = "cutter"
  hash_key         = "id"
  stream_enabled   = true
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attributes = [
    {
      name = "id"
      type = "S"
    }
  ]

  replica_regions = [
    "eu-west-2"
  ]

  tags = {
    Terraform   = "true"
    Environment = "prod"
  }
}

module "iam_assumable_role_ecs" {
  source  = "terraform-aws-modules/iam/aws//modules/iam-assumable-role"
  version = "3.13.0"

  trusted_role_services = [
    "ecs-tasks.amazonaws.com"
  ]

  create_role = true

  role_name         = "ecsTaskRole"
  role_requires_mfa = false

  custom_role_policy_arns = [
    "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy",
    "arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess",
    "arn:aws:iam::aws:policy/AmazonSSMReadOnlyAccess"
  ]
}

module "cutter_us_west_2" {
  source       = "./modules/cutter"
  region       = "us-west-2"
  azs          = ["us-west-2a", "us-west-2b"]
  hashids_salt = var.hashids_salt
  providers = {
    aws = aws
  }
  depends_on = [module.iam_assumable_role_ecs]
}

module "cutter_eu_west_2" {
  source       = "./modules/cutter"
  region       = "eu-west-2"
  azs          = ["eu-west-2a", "eu-west-2b"]
  hashids_salt = var.hashids_salt
  providers = {
    aws = aws.eu_west_2
  }
  depends_on = [module.iam_assumable_role_ecs]
}

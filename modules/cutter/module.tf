terraform {
  required_version = ">= 0.13"
}

data "aws_route53_zone" "cutt_click" {
  name         = "cutt.click"
  private_zone = false
}

data "aws_iam_role" "ecs" {
  name = "ecsTaskRole"
}

resource "aws_route53_record" "cutt_click" {
  zone_id = data.aws_route53_zone.cutt_click.zone_id
  name    = data.aws_route53_zone.cutt_click.name
  type    = "A"

  alias {
    name                   = module.alb.this_lb_dns_name
    zone_id                = module.alb.this_lb_zone_id
    evaluate_target_health = true
  }

  latency_routing_policy {
    region = var.region
  }

  set_identifier = var.region
}

data "aws_security_group" "default" {
  name   = "default"
  vpc_id = module.vpc.vpc_id
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.77.0"

  name                          = "main"
  cidr                          = "10.0.0.0/16"
  manage_default_security_group = true
  default_security_group_ingress = [
    {
      protocol  = -1
      self      = true
      from_port = 0
      to_port   = 0
    },
    {
      description = "https-443-tcp"
      from_port   = 443
      to_port     = 443
      protocol    = "tcp"
      cidr_blocks = "0.0.0.0/0"
    },
    {
      description = "http-80-tcp"
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      cidr_blocks = "0.0.0.0/0"
    },
    {
      description = "http-8080-tcp"
      from_port   = 8080
      to_port     = 8080
      protocol    = "tcp"
      cidr_blocks = "0.0.0.0/0"
    },
  ]
  default_security_group_egress = [
    {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = "0.0.0.0/0"
    }
  ]
  azs             = var.azs
  private_subnets = ["10.0.1.0/24", "10.0.2.0/24"]
  public_subnets  = ["10.0.101.0/24", "10.0.102.0/24"]

  enable_dns_hostnames = true
  enable_dns_support   = true

  enable_s3_endpoint       = true
  enable_dynamodb_endpoint = true

  # VPC Endpoint for ECR API
  enable_ecr_api_endpoint              = true
  ecr_api_endpoint_private_dns_enabled = true
  ecr_api_endpoint_security_group_ids  = [data.aws_security_group.default.id]

  # VPC Endpoint for ECR DKR
  enable_ecr_dkr_endpoint              = true
  ecr_dkr_endpoint_private_dns_enabled = true
  ecr_dkr_endpoint_security_group_ids  = [data.aws_security_group.default.id]

  # VPC Endpoint for CloudWatch Logs
  enable_logs_endpoint              = true
  logs_endpoint_private_dns_enabled = true
  logs_endpoint_security_group_ids  = [data.aws_security_group.default.id]

  # VPC Endpoint for Parameter Store
  enable_ssm_endpoint              = true
  ssm_endpoint_private_dns_enabled = true
  ssm_endpoint_security_group_ids  = [data.aws_security_group.default.id]

  tags = {
    Terraform   = "true"
    Environment = "prod"
  }
}

module "acm" {
  source      = "terraform-aws-modules/acm/aws"
  version     = "2.14.0"
  domain_name = data.aws_route53_zone.cutt_click.name
  zone_id     = data.aws_route53_zone.cutt_click.zone_id

  subject_alternative_names = [
    "*.${data.aws_route53_zone.cutt_click.name}",
  ]

  wait_for_validation = true

  tags = {
    Name = data.aws_route53_zone.cutt_click.name
  }
}

module "sg" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "3.18.0"

  name        = "alb-cutter"
  description = "Security group for cutter usage with ALB"
  vpc_id      = module.vpc.vpc_id

  ingress_cidr_blocks = ["0.0.0.0/0"]
  ingress_rules       = ["http-80-tcp", "https-443-tcp", "all-icmp"]
  egress_rules        = ["all-all"]
}

module "alb" {
  source  = "terraform-aws-modules/alb/aws"
  version = "5.12.0"

  name = "cutter"

  load_balancer_type = "application"

  vpc_id          = module.vpc.vpc_id
  security_groups = [module.sg.this_security_group_id]
  subnets         = module.vpc.public_subnets

  http_tcp_listeners = [
    {
      port        = 80
      protocol    = "HTTP"
      action_type = "redirect"
      redirect = {
        port        = "443"
        protocol    = "HTTPS"
        status_code = "HTTP_301"
      }
    },
  ]

  https_listeners = [
    {
      port            = 443
      protocol        = "HTTPS"
      certificate_arn = module.acm.this_acm_certificate_arn
    },
  ]

  target_groups = [
    {
      name                 = "cutter"
      backend_protocol     = "HTTP"
      backend_port         = 8080
      target_type          = "ip"
      deregistration_delay = 10
      health_check = {
        enabled             = true
        interval            = 30
        path                = "/"
        port                = "traffic-port"
        healthy_threshold   = 3
        unhealthy_threshold = 3
        timeout             = 5
        protocol            = "HTTP"
        matcher             = "200"
      }
    },
  ]

  tags = {
    Name = "cutter"
  }
}

resource "aws_ecs_cluster" "ecs" {
  name = "prime"
}

resource "aws_cloudwatch_log_group" "cutter" {
  name              = "/ecs/cutter-task-definition"
  retention_in_days = 30
}
# only for create. revision update by gitlab-ci
resource "aws_ecs_task_definition" "cutter" {
  family                   = "cutter"
  task_role_arn            = data.aws_iam_role.ecs.arn
  execution_role_arn       = data.aws_iam_role.ecs.arn
  network_mode             = "awsvpc"
  cpu                      = "512"
  memory                   = "1024"
  requires_compatibilities = ["FARGATE"]
  container_definitions = jsonencode([
    {
      name      = "cutter"
      image     = "923553073565.dkr.ecr.${var.region}.amazonaws.com/cutter:0.1.0"
      essential = true
      portMappings = [
        {
          containerPort = 8080
          hostPort      = 8080
        }
      ]
      environment = [
        {
          name  = "REGION_NAME",
          value = var.region
        }
      ]
      logConfiguration = {
        logDriver = "awslogs"
        options = {
          awslogs-group         = aws_cloudwatch_log_group.cutter.name
          awslogs-region        = var.region
          awslogs-stream-prefix = "ecs"
        }
      }
    }
  ])
  lifecycle {
    ignore_changes = [container_definitions]
  }
}
# only for create. revision update by gitlab-ci
resource "aws_ecs_service" "cutter" {
  name                               = "cutter"
  cluster                            = aws_ecs_cluster.ecs.id
  task_definition                    = aws_ecs_task_definition.cutter.arn
  desired_count                      = 2
  enable_ecs_managed_tags            = true
  launch_type                        = "FARGATE"
  deployment_maximum_percent         = 200
  deployment_minimum_healthy_percent = 100

  deployment_circuit_breaker {
    enable   = true
    rollback = true
  }

  load_balancer {
    target_group_arn = module.alb.target_group_arns.0
    container_name   = "cutter"
    container_port   = 8080
  }

  network_configuration {
    subnets         = module.vpc.private_subnets
    security_groups = [data.aws_security_group.default.id]
  }
  lifecycle {
    ignore_changes = all
  }
}

resource "aws_ssm_parameter" "cutter_hashids_salt" {
  name  = "/cutter/hashids_salt"
  type  = "SecureString"
  value = var.hashids_salt
}

variable "region" {
  type        = string
  description = "Region name"
}

variable "azs" {
  type        = list(string)
  description = "AZs for VPC"
}

variable "hashids_salt" {
  type        = string
  description = "Secret salt for hashids"
  sensitive   = true
}

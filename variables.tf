variable "hashids_salt" {
  type        = string
  description = "Secret salt for hashids"
  sensitive   = true
}
